package com.example.nezamulislamar.phonenetconnectivity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btn;
    TextView tv1,tv2;
    Context context;

    String TAG = "PhoneActivityTAG";
    Activity activity = MainActivity.this;
    Context cont = MainActivity.this;
    String wantPermission = Manifest.permission.READ_PHONE_STATE;
    private static final int PERMISSION_REQUEST_CODE = 1;
    String phnNumber = " ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button)findViewById(R.id.button1);
        tv1 = (TextView)findViewById(R.id.textView1);
        tv2 = (TextView) findViewById(R.id.numberPhn);
        context = MainActivity.this;

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                ConnectivityManager connectivitymanager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo[] networkInfo = connectivitymanager.getAllNetworkInfo();

                for (NetworkInfo netInfo : networkInfo) {

                    Log.d("net info print : \n", netInfo.getTypeName().toString());
                    try{

                        if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) {

                            if (netInfo.isConnected()) {

                                tv1.setText("Connected to WiFi");
                                tv2.setText("State -> Wifi (sim info false)");
                            }
                        }if (netInfo.getTypeName().equalsIgnoreCase("MOBILE")) {

                            if (netInfo.isConnected()) {
                                try{
                                    if (!checkPermission(wantPermission)) {
                                        requestPermission(wantPermission);
                                        boolean per = callUpperApiMethod();
                                        if(per == true){
                                            phnNumber += "permission true";
                                            tv2.setText(phnNumber);
                                        }else {
                                            tv2.setText("problem getting permission");
                                        }
                                    }else {
//                                        Log.d(TAG, "Phone number: " + getPhone());
//                                        phnNumber = "Phone number: \n" + getPhone();
//                                        tv2.setText("Mobile Data "+phnNumber);
                                        boolean mob = callUpperApiMethod();
                                    }
                                }catch (Exception e){
                                    tv2.setText("ERROR ... getting phn ");
                                }
                            }
                            tv1.setText("Connected to Mobile Data ");
                        }
                    }catch(Exception e){
                        tv1.setText("ERROR ... getting phn Network");
                    }
                }

            }
        });
    }

    private boolean checkPermission(String permission){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
//            int result = ContextCompat.checkSelfPermission(activity, permission);
//            if (result == PackageManager.PERMISSION_GRANTED){
//                return true;
//            } else {
//                return false;
//            }
            boolean res = callUpperApiMethod();
            return res;
        } else {
            return true;
        }
    }

    private void requestPermission(String permission){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)){
            Toast.makeText(activity, "Phone state permission allows us to get phone number. Please allow it for additional functionality.", Toast.LENGTH_LONG).show();
        }
        ActivityCompat.requestPermissions(activity, new String[]{permission},PERMISSION_REQUEST_CODE);
    }

    private String getPhone() {
        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(activity, wantPermission) != PackageManager.PERMISSION_GRANTED) {
            return "getPhone Not permitted";
        }
        //return phoneMgr.getLine1Number(); //this method
        String norm = phoneMgr.getLine1Number()+" detail "+callUpperApiMethod();
        return norm;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Phone number: " + getPhone());
                } else {
                    Toast.makeText(activity,"Permission Denied. We can't get phone number.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean callUpperApiMethod() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                SubscriptionManager sManager = (SubscriptionManager) cont.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                SubscriptionInfo infoSim1 = sManager.getActiveSubscriptionInfoForSimSlotIndex(0);
                SubscriptionInfo infoSim2 = sManager.getActiveSubscriptionInfoForSimSlotIndex(1);
                boolean infRSim1 = sManager.isNetworkRoaming(0);
                boolean infRSim2 = sManager.isNetworkRoaming(1);

//            List<SubscriptionInfo> subscription = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
//            for (int i = 0; i < subscription.size(); i++) {
//                SubscriptionInfo info = subscription.get(i);
                //phnNumber += infoSim2.getNumber().toString();
//                phnNumber = " sim1num: "+infoSim1.getNumber() +" sim2num : "+infoSim2.getNumber() +" sim1sub : "+infoSim1.getSubscriptionId() +" sim2sub : "+infoSim2.getSubscriptionId()
//                        +" sim1name : "+infoSim1.getDisplayName() +" sim2name : "+infoSim2.getDisplayName()+" sim1Car : "+infoSim1.getCarrierName() +" sim2Car : "+infoSim2.getCarrierName()
//                        +" sim1Iccid : "+infoSim1.getIccId() +" sim2IccId : "+infoSim2.getIccId()+" sim1Mcc : "+infoSim1.getMcc() +" sim2Mcc : "+infoSim2.getMcc()
//                        +" sim1Slot : "+infoSim1.getSimSlotIndex() +" sim2Slot : "+infoSim2.getSimSlotIndex()+" sim1Mns : "+infoSim1.getMnc() +" sim2Mnc : "+infoSim2.getMnc()
//                        +" sim1Ciso : "+infoSim1.getCountryIso() +" sim2Ciso : "+infoSim2.getCountryIso()+" sim1Droaming : "+infoSim1.getDataRoaming() +" simDMroaming : "+infoSim2.getDataRoaming()
//                        +" sim1Itint : "+infoSim1.getIconTint() +" sim2Itint : "+infoSim2.getIconTint();
                //NetworkInfo info = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                phnNumber = "sim1Slot"+infoSim1.getSimSlotIndex() +" sim2Slot : "+infoSim2.getSimSlotIndex()+" sim1Mnc : "+infoSim1.getMnc() +" sim2Mnc : "+infoSim2.getMnc()
                        +" sim1Ciso : "+infoSim1.getCountryIso() +" sim2Ciso : "+infoSim2.getCountryIso()+" sim1Droaming : "+infoSim1.getDataRoaming() +" simDMroaming : "+infoSim2.getDataRoaming()
                        +" sim1Itint : "+infoSim1.getIconTint() +" sim2Itint : "+infoSim2.getIconTint()+" isNet1 : "+infRSim1+" isNet2 : "+infRSim2;

//                phnNumber += info.getNumber();
//                Log.d(TAG, "number " + info.getNumber());
//                Log.d(TAG, "network name : " + info.getCarrierName());
//                Log.d(TAG, "country iso " + info.getCountryIso());
                tv2.setText("upper api : " + phnNumber);

            }catch (Exception e){
                tv2.setText("Upper api Error");
            }
            return true;
        }else{
            tv2.setText("CallUpperAPINotGetting...");
            return false;
        }
    }

}